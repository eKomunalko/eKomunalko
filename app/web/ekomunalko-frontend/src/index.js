import React from 'react';
import ReactDOM from 'react-dom';
import AppContainer from './containers/AppContainer';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import './styles/master.css';
import registerServiceWorker from './registerServiceWorker';


ReactDOM.render(<AppContainer />, document.getElementById('root'));
registerServiceWorker();
