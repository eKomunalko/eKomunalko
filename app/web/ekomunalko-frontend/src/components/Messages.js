import React, {Component} from 'react';

class Messages extends Component {
    render() {
        return (
            <li className="nav-item">
                <a className="nav-link" href={this.props.navItemLink}>{this.props.navItemText}</a>
            </li>
        );
    }
}

export default Messages;