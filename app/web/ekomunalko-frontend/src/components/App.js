import React, {Component} from 'react';
import NavBar from './NavBar';
import Main from "./Main";

class App extends Component {
    render() {
        return (
            <div>
                <NavBar navItems={[{"navItemText":"Prijavljeno", "navItemLink":"/prijavljeno"},
                                    {"navItemText":"Administratori", "navItemLink":"/administratori"},
                                    {"navItemText":"Kategorije", "navItemLink":"/kategorije"}]}
                                    // {"navItemText":"Poruke", "navItemLink":"/poruke"},
                                    // {"navItemText":"Pomoć", "navItemLink":"/pomoc"}]}
                        dropdownItems={[{"dropdownItemText":"Uredi osobne podatke", "dropdownItemLink":"#"},
                                            {"dropdownItemText":"Promijeni lozinku", "dropdownItemLink":"#"},
                                            {"dropdownItemText":"Odjavi se", "dropdownItemLink":"#"}]}
                        adminName="Marko Marković" />

                <Main loginFormFieldItems={[{"formFieldId":"loginUsername", "formFieldType":"text", "formFieldName":"username", "formFieldLabel":"Korisničko ime", "formFieldPlaceholder":"Upišite korisničko ime"},
                                            {"formFieldId":"loginPassword", "formFieldType":"password", "formFieldName":"password", "formFieldLabel":"Lozinka", "formFieldPlaceholder":"Upišite lozinku"}]}
                      entries={this.props.entries}
                      categories={this.props.categories}
                      // handleEntry={this.props.handleEntry}
                      entry={this.props.entry}
                      adminAddFormFieldItems={[{"formFieldId":"adminFirstName", "formFieldLabel":"Ime", "formFieldType":"text", "formFieldPlaceholder":"Upišite ime administratora"},
                                                {"formFieldId":"adminLastName", "formFieldLabel":"Prezime", "formFieldType":"text", "formFieldPlaceholder":"Upišite prezime administratora"},
                                                {"formFieldId":"adminEmail", "formFieldLabel":"Email adresa", "formFieldType":"email", "formFieldPlaceholder":"Upišite email adresu administratora"},
                                                {"formFieldId":"adminPhoneNumber", "formFieldLabel":"Broj telefona", "formFieldType":"number", "formFieldPlaceholder":"Upišite broj telefona administratora"}]}
                      categoryAddFormFieldItems={[{"formFieldId":"categoryName", "formFieldLabel":"Broj telefona", "formFieldType":"number", "formFieldPlaceholder":"Upišite broj telefona administratora"}]}  />
            </div>
        );
    }
}

export default App;
