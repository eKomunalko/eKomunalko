import React, {Component} from 'react';
import DropdownItem from './DropdownItem';

class NavDropdown extends Component {
    render() {
        return (
            <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" id="userDropdownMenuLink"
                   aria-haspopup="true" aria-expanded="false">{this.props.adminName} <span className="caret"> </span>
                </a>
                <div className="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="userDropdownMenuLink">
                    {this.props.dropdownItems.map((dropdownItem) =>
                        <DropdownItem key={dropdownItem.dropdownItemText} dropdownItemLink={dropdownItem.dropdownItemLink} dropdownItemText={dropdownItem.dropdownItemText} />
                    )}
                </div>
            </li>
        );
    }
}

export default NavDropdown;