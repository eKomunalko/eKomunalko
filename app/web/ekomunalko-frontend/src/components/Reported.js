import React, {Component} from 'react';
import ReactTable from "react-table";
import "react-table/react-table.css";
import '../styles/reported.css';

class Reported extends Component {
    uploadedFunc() {
        var x = document.getElementById("dateupl");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    render() {
        return (
            <div className="container-fluid" id="reported" >
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12">
                        <ReactTable
                            data={this.props.entries.map(entry => {
                                return entry;
                            })}
                            getTrProps = {(state, rowInfo, column, instance) => ({
                                onClick: e => window.location.href = "prijavljeno/" + rowInfo.row.id
                            })}
                            filterable
                            defaultFilterMethod={(filter, row) =>
                                    String(row[filter.id]) === filter.value}
                            columns={[
                                {
                                    Header: "#",
                                    accessor: "id",
                                    style: {cursor:'pointer'}
                                },
                                {
                                    Header: "Adresa",
                                    accessor: "address",
                                    style: {cursor:'pointer'},
                                    filterMethod: (filter, row) =>
                                        row[filter.id].toLowerCase().startsWith(filter.value)
                                },
                                {
                                    Header: "Opis",
                                    accessor: "description",
                                    style: {cursor:'pointer'},
                                    filterMethod: (filter, row) =>
                                        row[filter.id].toLowerCase().includes(filter.value)
                                },
                                {
                                    Header: "Kategorija",
                                    accessor: "category.name",
                                    style: {cursor:'pointer'},
                                    filterMethod: (filter, row) => {
                                        {let categories = this.props.categories;
                                            console.log(categories);
                                            let len = categories.length;
                                            for (let i = 0; i < len; i++) {
                                                if (filter.value === categories[i].name) {
                                                    return row[filter.id] === categories[i].name;
                                                }
                                        }
                                        return true;
                                        }
                                    },
                                    Filter: ({ filter, onChange }) =>
                                        <select
                                            onChange={event => onChange(event.target.value)}
                                            style={{ width: "100%" }}
                                            value={filter ? filter.value : "all"}
                                        >
                                            <option value="all">Prikaži sve</option>
                                            {this.props.categories.map((category) => (
                                                <option key={category.id} value={category.name}>{category.name}</option>
                                            ))}
                                        </select>
                                },
                                {
                                    Header: "Status",
                                    accessor: "status.name",
                                    style: {cursor:'pointer'},
                                    filterMethod: (filter, row) => {
                                        if (filter.value === "all") {
                                            return true;
                                        }
                                        if (filter.value === "received") {
                                            return row[filter.id] === "Zaprimljeno";
                                        }
                                        if (filter.value === "working") {
                                            return row[filter.id] === "U obradi";
                                        }
                                        if (filter.value === "solved") {
                                            return row[filter.id] === "Riješeno";
                                        }
                                    },
                                    Filter: ({ filter, onChange }) =>
                                        <select
                                            onChange={event => onChange(event.target.value)}
                                            style={{ width: "100%" }}
                                            value={filter ? filter.value : "all"}
                                        >
                                            <option value="all">Prikaži sve</option>
                                            <option value="received">Zaprimljeno</option>
                                            <option value="working">U obradi</option>
                                            <option value="solved">Riješeno</option>
                                        </select>
                                },
                                {
                                    Header: "Glasovi",
                                    accessor: "upvotes",
                                    style: {cursor:'pointer'}
                                },
                                {
                                    Header: "Zaprimljeno",
                                    accessor: "date_uploaded",
                                    style: {cursor:'pointer'},
                                    filterMethod: (filter, row) =>
                                        row[filter.id].startsWith(filter.value)
                                },
                                {
                                    Header: "Riješeno",
                                    accessor: "date_resolved",
                                    style: {cursor:'pointer'},
                                    filterMethod: (filter, row) =>
                                        row[filter.id].startsWith(filter.value)
                                }
                            ]}
                            className="-striped -highlight"
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default Reported;