import React, {Component} from 'react';
import TableButton from './TableButton';
import faEdit from '@fortawesome/fontawesome-free-regular/faEdit';
import faTrash from '@fortawesome/fontawesome-free-regular/faTrashAlt';

class CategoriesTableRow extends Component {
    render() {
        return (
            <tr>
                <td>{this.props.id}</td>
                <td>{this.props.category}</td>
                {/*<td>*/}
                    {/*<TableButton buttonClass="btn-edit" buttonTitle="Uredi" buttonIcon={faEdit} />*/}
                {/*</td>*/}
                {/*<td>*/}
                    {/*<TableButton buttonClass="btn-delete" buttonTitle="Obriši" buttonIcon={faTrash} />*/}
                {/*</td>*/}
            </tr>
        );
    }
}

export default CategoriesTableRow;