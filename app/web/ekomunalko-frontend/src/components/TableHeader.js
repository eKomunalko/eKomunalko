import React, {Component} from 'react';

class TableHeader extends Component {
    render() {
        return (
            <thead>
                <tr>
                    {this.props.columnNames.map((columnName) => (
                        <th scope="col" key={columnName}>{columnName}</th>
                    ))}
                </tr>
            </thead>
        );
    }
}

export default TableHeader;