import React, {Component} from 'react';

class FormField extends Component {
    render() {
        return (
            <div className="form-group">
                <label htmlFor={this.props.formFieldId}>{this.props.formFieldLabel}</label>
                <input type={this.props.formFieldType} name={this.props.formFieldName} className="form-control" id={this.props.formFieldId}
                       placeholder={this.props.formFieldPlaceholder}/>
            </div>
        );
    }
}

export default FormField;