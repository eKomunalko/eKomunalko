import React, {Component} from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faPlus from "@fortawesome/fontawesome-free-solid/faPlus";
import '../styles/add-button.css';

class AddButton extends Component {
    render() {
        return (
            <a className="btn btn-outline-success" href={this.props.buttonLink} id="addButton">
                <FontAwesomeIcon icon={faPlus} size="lg" /> {this.props.buttonText}
            </a>
        );
    }
}

export default AddButton;