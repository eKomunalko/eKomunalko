import React, {Component} from 'react';
import FormField from './FormField';
import FormSubmitButton from './FormSubmitButton';

class Form extends Component {
    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-xl-6 col-lg-7 col-md-8 col-sm-10 col-10">
                        <form role="form" action="" method="POST">
                            {this.props.formFieldItems.map((formFieldItem) => (
                                <FormField key={formFieldItem.formFieldId}
                                           formFieldId={formFieldItem.formFieldId}
                                           formFieldLabel={formFieldItem.formFieldLabel}
                                           formFieldType={formFieldItem.formFieldType} formFieldPlaceholder />
                                ))}

                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Form;