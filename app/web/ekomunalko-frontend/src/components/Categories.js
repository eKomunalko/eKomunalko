import React, {Component} from 'react';
import TableHeader from './TableHeader';
import CategoriesTableRow from './CategoriesTableRow';
import AddButton from "./AddButton";
import '../styles/categories.css';

class Categories extends Component {
    render() {
        return (
            <div className="container-fluid" id="categories">
                <div className="row">
                    {/*<div className="col-xl-2 col-lg-2 col-md-2">*/}
                        {/*<AddButton buttonText="Dodaj kategoriju" buttonLink="/kategorije/dodaj" />*/}
                    {/*</div>*/}

                    <div className="col-xl-12 col-lg-12 col-md-12">
                        <table className="table table-bordered table-striped table-hover">
                            <TableHeader columnNames={["#", "Naziv"]} />
                            <tbody>
                                {this.props.categories.map((category) => (
                                    <CategoriesTableRow key={category.id}
                                                        id={category.id}
                                                        category={category.name} />
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default Categories;