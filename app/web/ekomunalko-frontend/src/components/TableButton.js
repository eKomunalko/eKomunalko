import React, {Component} from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

class TableButton extends Component {
    render() {
        return (
            <button className={"btn btn-sm " + this.props.buttonClass} title={this.props.buttonTitle}>
                <FontAwesomeIcon icon={this.props.buttonIcon} size="2x" />
            </button>
        );
    }
}

export default TableButton;