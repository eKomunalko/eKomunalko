import React, {Component} from 'react';
import TableHeader from './TableHeader';
import AdminsTableRow from "./AdminsTableRow";
import TableButton from './TableButton';
import AddButton from './AddButton';
import faTrash from '@fortawesome/fontawesome-free-regular/faTrashAlt';
import '../styles/admins.css';

class Admins extends Component {
    render() {
        return (
            <div className="container-fluid" id="admins">
                <div className="row">
                    {/*<div className="col-xl-2 col-lg-2 col-md-2">*/}
                        {/*<AddButton buttonText="Dodaj administratora" buttonLink="/administratori/dodaj" />*/}
                    {/*</div>*/}

                    <div className="col-xl-12 col-lg-12 col-md-12">
                        <table className="table table-bordered table-striped table-hover">
                            <TableHeader columnNames={["#", "Ime", "Prezime", "Email", "Broj telefona", "Kategorije", "Aktivan"]} />
                            <tbody>
                                {/*{this.props.entries.map((admin) => (*/}
                                    {/*<AdminsTableRow key={admin.id}*/}
                                                      {/*id={admin.id}*/}
                                                      {/*first_name={admin.first_name}*/}
                                                      {/*last_name={admin.last_name}*/}
                                                      {/*email={admin.email}*/}
                                                      {/*phone_number={admin.phone_number} />*/}
                                {/*))}*/}
                                <tr>
                                    <td>1</td>
                                    <td>Marko</td>
                                    <td>Marković</td>
                                    <td>mmarkovic@mail.com</td>
                                    <td>0911234567</td>
                                    <td>Ceste</td>
                                    <td>Da</td>
                                    {/*<td>*/}
                                        {/*<TableButton buttonClass="btn-delete" buttonTitle="Obriši" buttonIcon={faTrash} />*/}
                                    {/*</td>*/}
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Ivica</td>
                                    <td>Ivić</td>
                                    <td>iivic@mail.com</td>
                                    <td>0917654321</td>
                                    <td>Zgrade, parkovi</td>
                                    <td>Ne</td>
                                    {/*<td>*/}
                                        {/*<TableButton buttonClass="btn-delete" buttonTitle="Obriši" buttonIcon={faTrash} />*/}
                                    {/*</td>*/}
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default Admins;