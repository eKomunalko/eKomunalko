import React, {Component} from 'react';
import TableButton from './TableButton';
import faPrint from '@fortawesome/fontawesome-free-solid/faPrint';
import faEnvelope from '@fortawesome/fontawesome-free-regular/faEnvelope';


class ReportedTableRow extends Component {
    render() {
        return (
            <tr>
                {/*<td onClick={() => this.props.handleEntry(this.props.id)}>{this.props.id}</td>*/}
                <td onClick={() => window.location.href = 'prijavljeno/' + this.props.id}>{this.props.id}</td>
                <td>{this.props.address}</td>
                <td>{this.props.description}</td>
                <td>{this.props.category}</td>
                <td>{this.props.status}</td>
                <td>{this.props.upvotes}</td>
                <td>{this.props.date_uploaded}</td>
                <td>{this.props.date_resolved}</td>
                <td>
                    <TableButton buttonClass="btn-print" buttonTitle="Ispiši" buttonIcon={faPrint} />
                </td>
                {/*<td>*/}
                    {/*<TableButton buttonClass="btn-message" buttonTitle="Proslijedi" buttonIcon={faEnvelope} />*/}
                {/*</td>*/}
            </tr>
        );
    }
}

export default ReportedTableRow;