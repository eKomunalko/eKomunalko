import React, {Component} from 'react';
import FormField from './FormField';
import FormSubmitButton from './FormSubmitButton';
import CSRFToken from './CSRFToken';
import '../styles/login.css';

class Login extends Component {
    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-xl-6 col-lg-7 col-md-8 col-sm-10 col-10">
                        <form id="loginForm" action="/api/login" method="POST">
                            <CSRFToken />
                            {this.props.formFieldItems.map((formFieldItem) => (
                                <FormField key={formFieldItem.formFieldId}
                                           formFieldType={formFieldItem.formFieldType}
                                           formFieldName={formFieldItem.formFieldName}
                                           formFieldId={formFieldItem.formFieldId}
                                           formFieldLabel={formFieldItem.formFieldLabel}
                                           formFieldPlaceholder={formFieldItem.formFieldPlaceholder} />
                            ))}
                            <FormSubmitButton submitText="Prijavi se" />
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;