import React, {Component} from 'react';
import '../styles/form-submit-button.css'

class FormSubmitButton extends Component {
    render() {
        return (
            <div className="text-center">
                <button type="submit" className="btn btn-success" id="submitButton">{this.props.submitText}</button>
            </div>
        );
    }
}

export default FormSubmitButton;