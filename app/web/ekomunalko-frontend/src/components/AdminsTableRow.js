import React, {Component} from 'react';
import TableButton from './TableButton';
import faTrash from '@fortawesome/fontawesome-free-regular/faTrashAlt';

class AdminsTableRow extends Component {
    render() {
        return (
            <tr>
                <td>{this.props.id}</td>
                <td>{this.props.firstName}</td>
                <td>{this.props.lastName}</td>
                <td>{this.props.email}</td>
                <td>{this.props.phoneNumber}</td>
                {/*<td>{this.props.categories}</td>*/}
                <td>{this.props.active}</td>
                {/*<td>*/}
                    {/*<TableButton buttonClass="btn-delete" buttonTitle="Obriši" buttonIcon={faTrash} />*/}
                {/*</td>*/}
            </tr>
        );
    }
}

export default AdminsTableRow;