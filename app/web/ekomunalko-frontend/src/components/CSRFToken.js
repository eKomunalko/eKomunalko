import React, {Component} from 'react';
import jQuery from 'jquery';

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        let cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            let cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

let csrftoken = getCookie("csrftoken");

class CSRFToken extends Component {
    render() {
        return (
            <input type="hidden" name="csrfmiddlewaretoken" value={csrftoken} />
        );
    }
}

export default CSRFToken;