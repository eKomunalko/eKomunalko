/* global google */
import React, {Component} from 'react';
// import Map from './Map';
import '../styles/entry.css';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faAngleLeft from '@fortawesome/fontawesome-free-solid/faAngleLeft';
import faAngleRight from '@fortawesome/fontawesome-free-solid/faAngleRight';
import faArrowLeft from '@fortawesome/fontawesome-free-solid/faArrowLeft';
import axios from 'axios';
import jQuery from 'jquery';
/* global $ */
var $ = jQuery;



// const google = window.google;
// async function initMap(address) {
//     if (address != undefined) {
//         let address_data = await axios.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + address.replace(new RegExp(' ', 'g'), '%20') + '&key=AIzaSyAKD68r16EH5_AjSsHcHQzuPZFzTgCu8tc');
//         // this.setState({address_data: address_data.data})
//         console.log(address_data);
//         let latitude = address_data.data.results[0].geometry.location.lat;
//         let longitude = address_data.data.results[0].geometry.location.lng;
//         let options = {
//             zoom: 17,
//             center: {lat: latitude, lng: longitude}
//         };
//         let map = new google.maps.Map(document.getElementById('map'), options);
//         let marker = new google.maps.Marker({
//             position: {lat: latitude, lng: longitude},
//             map: map
//         });
//     }
// }

function loadScript(src) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    document.getElementsByTagName("head")[0].appendChild(script);
    script.src = src;
}

loadScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyAKD68r16EH5_AjSsHcHQzuPZFzTgCu8tc");


function initMap(address) {
    $.ajax({
        type: 'GET',
        url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address.replace(new RegExp(' ', 'g'), '%20') + '&key=AIzaSyAKD68r16EH5_AjSsHcHQzuPZFzTgCu8tc',
        success: (response) => {

            let lat = response.results[0].geometry.location.lat;
            let lng = response.results[0].geometry.location.lng;

            let options = {
                zoom: 17,
                center: {lat: lat, lng: lng}
            };
            console.log(google);
            let map = new window.google.maps.Map(document.getElementById('map'), options);
            console.log(map);
            let marker = new window.google.maps.Marker({
                position: {lat: lat, lng: lng},
                map: map
            });
        },
    });
}

class Entry extends Component {
    // constructor() {
    //     super();
    //     this.state = {
    //         address_data: {}
    //     };
    // }
    //
    // async componentDidMount() {
    //     try {
    //         let entry = await axios.get('/api/entries/' + window.location.href.split('prijavljeno/')[1] + '?format=json');
    //         this.setState({entry: entry.data});
    //     } catch (error) {
    //         console.log(error);
    //     }
    // };

    render() {
        return (
            <div className="container-fluid" id="entry">
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-xl-7 col-lg-7 col-md-7">
                                    <a href="/prijavljeno" role="button">
                                        <FontAwesomeIcon icon={faArrowLeft} size="2x" className="fa-arrow-left" />
                                        <span className="sr-only">Back</span>
                                    </a>
                                    <br/><br/>
                                    <h5>
                                        <b>ID prijave: </b>
                                        {this.props.entry.id}
                                    </h5>
                                    <br/>
                                    <h5>
                                        <b>Adresa: </b>
                                        {this.props.entry.address}
                                    </h5>
                                    <br/>
                                    <h5>
                                        <b>Kategorija: </b>
                                        {this.props.entry.category != undefined ? this.props.entry.category.name : '/'}
                                    </h5>
                                    <br/>
                                    <h5>
                                        <b>Status: </b>
                                            <select name="status">
                                                <option value={this.props.entry.status != undefined ? this.props.entry.status.name : '/'}>
                                                    {this.props.entry.status != undefined ? this.props.entry.status.name : '/'}</option>
                                                <option value="UObradi">U obradi</option>
                                                <option value="Riješeno">Riješeno</option>
                                            </select>
                                    </h5>
                                    <br/>
                                    <h5>
                                        <b>Opis: </b>
                                        {this.props.entry.description}
                                    </h5>
                                    <br/>
                                    <h5>
                                        <b>Datum i vrijeme prijave: </b>
                                        {this.props.entry.date_uploaded}
                                    </h5>
                                    <br/>
                                    <h5>
                                        <b>Datum i vrijeme popravka: </b>
                                        {this.props.entry.date_resolved != undefined ? this.props.entry.date_resolved : '/'}
                                    </h5>
                                </div>
                                <div className="col-xl-5 col-lg-5 col-md-5">
                                    <div id="map"></div>
                                    {this.props.entry.address != undefined ? initMap(this.props.entry.address) : 'Učitava se...'}
                                </div>
                            </div>
                            {/*<div className="row">*/}
                                {/*<div className="col-xl-12 col-lg-12 col-md-12 image d-flex justify-content-center text-center">*/}
                                    {/*<div id="photos" className="carousel slide " data-ride="carousel">*/}
                                        {/*<div className="carousel-inner">*/}
                                            {/*{this.props.entry.photos != undefined ? this.props.entry.photos.map((photo) => (*/}
                                                {/*<div className="carousel-item" key={photo.photo}>*/}
                                                    {/*<img src={"http://35.197.198.17:8080/" + photo.photo} />*/}
                                                {/*</div>*/}
                                            {/*)) : '/'}*/}
                                        {/*</div>*/}
                                        {/*<a className="carousel-control-prev" href="#photos" role="button" data-slide="prev">*/}
                                            {/*<FontAwesomeIcon icon={faAngleLeft} size="2x" className="fa-angle" />*/}
                                            {/*<span className="sr-only">Previous</span>*/}
                                        {/*</a>*/}
                                        {/*<a className="carousel-control-next" href="#photos" role="button" data-slide="next">*/}
                                            {/*<FontAwesomeIcon icon={faAngleRight} size="2x" className="fa-angle" />*/}
                                            {/*<span className="sr-only">Next</span>*/}
                                        {/*</a>*/}
                                    {/*</div>*/}
                                {/*</div>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Entry;