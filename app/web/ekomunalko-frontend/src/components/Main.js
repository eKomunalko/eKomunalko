import React, {Component} from 'react';
import { Switch, Route } from 'react-router-dom';
import Login from './Login';
import Reported from './Reported';
import Entry from './Entry';
import Admins from './Admins';
import AdminAddForm from './AdminAddForm';
import Categories from './Categories';
import CategoryAddForm from './CategoryAddForm';
import Messages from './Messages';
import Help from './Help';

class Main extends Component {
    render() {
        return (
            <Switch>
                <Route exact path='/' render={() => <Login formFieldItems={this.props.loginFormFieldItems} />} />
                <Route exact path='/prijavljeno' render={() => <Reported entries={this.props.entries} categories={this.props.categories}/>} />
                <Route path='/prijavljeno/:number' render={() => <Entry entry={this.props.entry} />} />
                <Route exact path='/administratori' component={Admins} />
                <Route exact path='/administratori/dodaj' render={() => <AdminAddForm formFieldItems={this.props.adminAddFormFieldItems} />} />
                <Route exact path='/kategorije' render={() => <Categories categories={this.props.categories} />} />
                <Route path='/kategorije/dodaj' component={CategoryAddForm} />
                {/*<Route path='/poruke' component={Messages} />*/}
                {/*<Route path='/pomoc' component={Help} />*/}
            </Switch>
        );
    }
}

export default Main;