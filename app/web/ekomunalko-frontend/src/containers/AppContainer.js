import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import axios from 'axios';
import App from '../components/App';


class AppContainer extends Component {
    constructor() {
        super();
        this.state = {
            entries: [],
            categories: [],
            entry: {}
        };
    }

    async componentDidMount() {
        try {
            let entries = await axios.get('/api/entries?format=json');
            let categories = await axios.get('/api/categories?format=json');
            let entry = await axios.get('/api/entries/' + window.location.href.split('prijavljeno/')[1] + '?format=json');
            await this.setState({entries: entries.data, categories: categories.data, entry: entry.data});
        } catch (error) {
            console.log(error);
        }
    }

    // handleEntry = async (id) => {
    //     try {
    //         window.location.href = 'prijavljeno/' + id;
    //         let entry = await axios.get('/api/entries/' + id + '?format=json');
    //         this.setState({entry: entry.data});
    //     } catch (error) {
    //         console.log(error);
    //     }
    // };

    render() {
        return (
            <BrowserRouter>
                <App entries={this.state.entries}
                     categories={this.state.categories}
                     // handleEntry={this.handleEntry}
                     entry={this.state.entry} />
            </BrowserRouter>
        );
    }
}

export default AppContainer;