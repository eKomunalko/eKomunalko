from django.urls import path
from administration.views import *
from administration.api import *

app_name = 'administration'

urlpatterns = [
    path('login', Login.as_view()),
    path('logout', Logout.as_view()),
    path('admins', AdminList.as_view()),
    path('cities', CityList.as_view()),
    path('categories', CategoryList.as_view()),
    path('entries', EntryList.as_view()),
    path('entries/<int:id>', EntryDetail.as_view()),
    # path('vote', EntryVote.as_view()),
    # path('status', EntryStatus.as_view())
]
