from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
import string, random
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.encoding import force_text, force_bytes
from django.utils.http import urlsafe_base64_encode
from administration.tokens import admin_registration_token
import sendgrid
from sendgrid.helpers.mail import *
from .forms import *
from .models import *
from django.views import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie


class Login(View):
    @method_decorator(ensure_csrf_cookie)
    def post(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/prijavljeno')
        else:
            return redirect('/administratori')


class Logout(View):
    def get(self, request):
        logout(request)
        return redirect('')


# def admin_login(request):
#     if request.method == 'POST':
#         login_form = Form(data=request.POST)
#         if login_form.is_valid():
#             user = authenticate(request, username=login_form.cleaned_data['username'],
#                                 password=login_form.cleaned_data['password'])
#             if user is not None:
#                 login(request, user)
#                 return redirect('prijavljeno/')
#     context = {'login_form': Form()}
#     return render(request, 'administration/reported.html', context)
#
#
# def admin_logout(request):
#     logout(request)
#     return redirect('')


def admin_add(request):
    if request.method == 'POST':
        add_admin_form = AddAdminForm(request.POST)
        if add_admin_form.is_valid():
            admin = add_admin_form.save(commit=False)

            username = admin.first_name[:1].lower() + admin.last_name.lower()
            username = username.replace('č', 'c').replace('ć', 'c').replace('đ', 'd') \
                .replace('š', 's').replace('ž', 'z')
            all_admins = Admin.objects.get(username__contains=username)
            if all_admins:
                username_unique = True
                number_string = ''
                number = 0
                while username_unique:
                    username_number = username + number_string
                    for existing_admin in all_admins:
                        if username_number == existing_admin.username:
                            number += 1
                            number_string = str(number)
                            username_unique = False
                            break
                    if username_unique:
                        admin.username = username_number
                        break
                    username_unique = True

            password = ''
            password_letters = 7
            password_numbers = 3
            for character in range(password_letters):
                password += random.choice(string.ascii_letters)
            for character in range(password_numbers):
                password += random.choice(string.digits)
            admin.set_password(''.join(random.sample(password, len(password))))

            admin.is_active = False
            admin.save()

            current_site = get_current_site(request)
            sg = sendgrid.SendGridAPIClient(
                apikey='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')
            from_email = Email() # email of main admin
            to_email = Email(admin.email)
            subject = "eKomunalko korisnički račun"
            message = render_to_string('administration/admin-invite-email.html', {
                'domain': current_site.domain,
                'uid': force_text(urlsafe_base64_encode(force_bytes(admin.pk))),
                'token': admin_registration_token.make_token(admin)
            })
            content = Content("text/html", message)
            mail = Mail(from_email, subject, to_email, content)
            sg.client.mail.send.post(request_body=mail.get())

            notification = 'Administratoru je poslan e-mail sa korisničkim imenom i lozinkom.'
            context = {'notification': notification}
            return render(request, 'administration/notification.html', context)
        else:
            notification = 'Dodavanje administratora nije uspjelo! Molimo Vas pokušajte ponovno'
            context = {'notification': notification,
                       'add_admin_form': AddAdminForm()}
            return render(request, 'administration/notification.html', context)
