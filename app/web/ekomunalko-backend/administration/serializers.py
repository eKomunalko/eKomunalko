from rest_framework import serializers
from .models import *
import datetime
from django.core.files.base import ContentFile
import base64
import six
import uuid
import imghdr


class Base64ImageField(serializers.ImageField):
    """
    A Django REST framework field for handling image-uploads through raw post data.
    It uses base64 for encoding and decoding the contents of the file.
    """

    def to_internal_value(self, data):
        # from django.core.files.base import ContentFile
        # import base64
        # import six
        # import uuid

        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')
            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')
            # Generate file name:
            file_name = str(uuid.uuid4())[:12]  # 12 characters are more than enough.
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)
            complete_file_name = "%s.%s" % (file_name, file_extension, )
            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        # import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('id', 'name', 'abbrev')


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name')


class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = ('id', 'name')


class PhotoSerializer(serializers.ModelSerializer):
    photo = Base64ImageField(max_length=None, use_url=True)

    class Meta:
        model = Photo
        fields = ('id', 'photo', 'is_before')


class EntrySerializer(serializers.ModelSerializer):
    city = CitySerializer()
    category = CategorySerializer()
    status = StatusSerializer()
    photos = PhotoSerializer(many=True)

    class Meta:
        model = Entry
        fields = ('id', 'address', 'description', 'city', 'category', 'status', 'photos', 'upvotes', 'date_uploaded',
                  'date_resolved')

    def create(self, validated_data):
        photos_data = validated_data.pop('photos')
        entry = Entry(
            address=validated_data['address'],
            description=validated_data['description'],
            city=City.objects.get(name=validated_data.pop('city')['name']),
            category=Category.objects.get(name=validated_data.pop('category')['name']),
            status=Status.objects.get(name='Zaprimljeno'),
            date_uploaded=datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        )
        entry.save()
        for photo_data in photos_data:
            photo = Photo(
                photo=photo_data.get('photo'),
                entry=Entry.objects.get(id=entry.id),
                is_before=True
            )
            photo.save()
        return entry


class EntryStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Entry
        fields = ('status',)

    def update(self, instance, validated_data):
        # instance.upvotes = validated_data.get('upvotes', instance.upvotes)
        status_data = validated_data.pop('status')
        instance.status = Status.objects.get(name=status_data.get('name', instance.status.name))
        if status_data['name'] == 'Riješeno':
            instance.date_resolved = validated_data.get('date_resolved', instance.date_resolved)
        instance.save()


class EntryVoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Entry
        fields = ('upvotes',)

    def update(self, instance, validated_data):
        instance.upvotes = validated_data.get('upvotes', instance.upvotes)
        return instance


class AdminSerializer(serializers.ModelSerializer):
    city = CitySerializer()
    categories = CategorySerializer(many=True)

    class Meta:
        model = Admin
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'phone_number', 'is_main_admin', 'city',
                  'categories')

    def create(self, validated_data):
        categories_data = validated_data.pop('categories')
        admin = Admin(
            username=validated_data['username'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            email=validated_data['email'],
            phone_number=validated_data['phone_number'],
            is_main_admin=validated_data['is_main_admin'],
            city=City.objects.get(name=validated_data.pop('city')['name']),
        )
        # category=Category.objects.get(name=validated_data.pop('category')['name']),
        for category in categories_data:
            category = Category.objects.get(name=category['name'])
            admin.categories.add(category)
        admin.save()
        return admin
