from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six


class AdminRegistrationTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, admin_email, timestamp):
        return six.text_type(admin_email) + six.text_type(timestamp)


admin_registration_token = AdminRegistrationTokenGenerator()
