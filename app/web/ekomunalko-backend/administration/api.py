from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import JSONParser, MultiPartParser, FormParser
from rest_framework import status
from .serializers import *
from .models import *
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt


class AdminList(APIView):
    def post(self, request):
        serializer = AdminSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CityList(generics.ListCreateAPIView):
    queryset = City.objects.all()
    serializer_class = CitySerializer


class CategoryList(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


@method_decorator(csrf_exempt, name='dispatch')
class EntryList(APIView):
    parser_classes = (JSONParser, MultiPartParser, FormParser,)

    @method_decorator(csrf_exempt)
    def get(self, request):
        amount = int(request.GET.get('amount', 0))  # 0 means all
        start_id = int(request.GET.get('start', 0))
        city_name = request.GET.get('city')
        if amount == 0:
            entries = Entry.objects.all().select_related('city', 'category', 'status').order_by('-id')
        else:
            entries = Entry.objects.filter(id__gt=start_id, city__name__exact=city_name).order_by('-id')[start_id:amount]
            print(entries)
        serializer = EntrySerializer(entries, many=True)
        return Response(serializer.data)

    @method_decorator(csrf_exempt)
    def post(self, request):
        serializer = EntrySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class EntryDetail(APIView):
    @method_decorator(csrf_exempt)
    def get(self, request, id):
        entry = Entry.objects.get(id=id)
        serializer = EntrySerializer(entry)
        return Response(serializer.data)


class EntryVote(APIView):
    @method_decorator(csrf_exempt)
    def patch(self, request):
        entry = Entry.objects.get(id=request.data['id'])
        serializer = EntryVoteSerializer(entry, request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class EntryStatus(APIView):
    @method_decorator(csrf_exempt)
    def put(self, request):
        entry = Entry.objects.get(id=request.data['id'])
        serializer = EntryStatusSerializer(entry, request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# class EntryVote(APIView):
#     def get(self, request):
#         view = EntryVotePUT
#         return view(request)
#         # entry = Entry.objects.get(id=request.GET.get('id'))
