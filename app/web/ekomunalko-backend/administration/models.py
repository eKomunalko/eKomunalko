from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from phonenumber_field.modelfields import PhoneNumberField


class City(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    abbrev = models.CharField(max_length=255)

    class Meta:
        db_table = 'city'


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)

    class Meta:
        db_table = 'category'


class Status(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=True)

    class Meta:
        db_table = 'status'


class Entry(models.Model):
    id = models.AutoField(primary_key=True)
    address = models.CharField(max_length=255)
    description = models.TextField()
    city = models.ForeignKey(City, on_delete=models.CASCADE, db_column='city_id')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, db_column='category_id')
    status = models.ForeignKey(Status, on_delete=models.CASCADE, db_column='status_id')
    upvotes = models.IntegerField(default=0)
    date_uploaded = models.DateTimeField(blank=True)
    date_resolved = models.DateTimeField(null=True)
    # user_id = models.ForeignKey('RegularUser', on_delete=models.CASCADE)

    class Meta:
        db_table = 'entry'


class Photo(models.Model):
    id = models.AutoField(primary_key=True)
    photo = models.ImageField(upload_to='photos/')
    entry = models.ForeignKey(Entry, related_name='photos', on_delete=models.CASCADE, db_column='entry_id')
    is_before = models.BooleanField()

    class Meta:
        db_table = 'photo'


class Admin(AbstractUser):
    email = models.EmailField(unique=True, max_length=255)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    phone_number = PhoneNumberField()
    is_main_admin = models.BooleanField(default=False)
    city = models.ManyToManyField(City, through='AdminCity')
    categories = models.ManyToManyField(Category, through='AdminCategory')

    REQUIRED_FIELDS = ['email', 'first_name', 'last_name']

    class Meta:
        db_table = 'admin'


class AdminCity(models.Model):
    id = models.AutoField(primary_key=True)
    admin_id = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, db_column='admin_id')
    city_id = models.ForeignKey(City, on_delete=models.CASCADE, db_column='city_id')

    class Meta:
        db_table = 'admincity'


class AdminCategory(models.Model):
    id = models.AutoField(primary_key=True)
    admin_id = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, db_column='admin_id')
    category_id = models.ForeignKey(Category, on_delete=models.CASCADE, db_column='category_id')

    class Meta:
        db_table = 'admincategory'


# class RegularUser(models.Model):
#     id = models.AutoField(primary_key=True)
#     email = models.CharField(max_length=255)
#     email_notifications = models.BooleanField(default=False)
#     notifications = models.BooleanField(default=False)
#     location = models.BooleanField(default=False)
#
#     class Meta:
#         db_table = 'regularuser'
