from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from .models import Admin
from phonenumber_field.formfields import PhoneNumberField


class Form(AuthenticationForm):
    email = forms.CharField(required=True, max_length=255, label="E-mail",
                            widget=forms.TextInput(
                                attrs={'placeholder': 'Upišite e-mail', 'class': 'form-control', 'id': 'loginEmail'}))
    password = forms.CharField(required=True, max_length=255, label="Lozinka",
                               widget=forms.PasswordInput(
                                   attrs={'placeholder': 'Upišite lozinku', 'class': 'form-control'}))


# maybe forms.Form
class AddAdminForm(UserCreationForm):
    first_name = forms.CharField(required=True, max_length=255, label="Ime",
                                 widget=forms.TextInput(
                                     attrs={'placeholder': 'Upišite ime', 'class': 'form-control'}))
    last_name = forms.CharField(required=True, max_length=255, label="Prezime",
                                widget=forms.TextInput(
                                    attrs={'placeholder': 'Upišite prezime', 'class': 'form-control'}))
    email = forms.EmailField(required=True, max_length=255, label="E-mail",
                             widget=forms.EmailInput(attrs={'placeholder': 'Upišite e-mail adresu administratora',
                                                            'class': 'form-control'}))
    phone_number = PhoneNumberField(max_length=30, label='Broj telefona',
                                    widget=forms.NumberInput(
                                        attrs={'placeholder': 'Broj telefona', 'class': 'form-control'}))
    # category =

    class Meta:
        model = Admin
        fields = ['first_name', 'last_name', 'email']

    def save(self, commit=True):
        user = super(AddAdminForm, self).save(commit=False)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']
        if commit:
            user.save()

        return user


# class RegistrationForm(UserCreationForm):
#     first_name = forms.CharField(required=True, max_length=255, label="Ime",
#                                  widget=forms.TextInput(
#                                      attrs={'placeholder': 'Upišite ime', 'class': 'form-control'}))
#     last_name = forms.CharField(required=True, max_length=255, label="Prezime",
#                                 widget=forms.TextInput(
#                                     attrs={'placeholder': 'Upišite prezime', 'class': 'form-control'}))
#     email = forms.EmailField(required=True, max_length=255, label="E-mail",
#                              widget=forms.EmailInput(
#                                  attrs={'placeholder': 'Upišite e-mail adresu', 'class': 'form-control'}))
#     password1 = forms.CharField(required=True, max_length=255, label="Lozinka",
#                                 widget=forms.PasswordInput(
#                                     attrs={'placeholder': 'Upišite lozinku', 'class': 'form-control'}))
#     password2 = forms.CharField(required=True, max_length=255,
#                                 widget=forms.PasswordInput(
#                                     attrs={'placeholder': 'Potvrdite lozinku', 'class': 'form-control'}))
#
#     class Meta:
#         model = Admin
#         fields = ['first_name', 'last_name', 'email', 'password1', 'password2']
#
#     def save(self, commit=True):
#         user = super(RegistrationForm, self).save(commit=False)
#         user.first_name = self.cleaned_data['first_name']
#         user.last_name = self.cleaned_data['last_name']
#         user.email = self.cleaned_data['email']
#         if commit:
#             user.save()
#
#         return user
