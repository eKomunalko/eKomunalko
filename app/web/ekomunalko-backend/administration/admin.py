from django.contrib import admin
from .models import *

admin.site.register(Admin)
admin.site.register(Entry)
admin.site.register(City)
admin.site.register(Category)
admin.site.register(Status)