package com.ministryofcode.ekomunalko;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ministryofcode.ekomunalko.api.ApiCall;
import com.ministryofcode.ekomunalko.api.Report;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Josip on 10.5.2018..
 */

public class ReportsListAdapter extends BaseAdapter {

    private Context mContext;
    private List<Report> reports;

    public List<Report> getReports() {
        return reports;
    }

    public ReportsListAdapter(Context context, List<Report> reports) {
        this.mContext = context;
        this.reports = reports;
    }

    @Override
    public int getCount() {
        return reports.size();
    }

    @Override
    public Object getItem(int i) {
        return reports.get(i);
    }

    @Override
    public long getItemId(int i) {
        return reports.get(i).getId();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        View view;
        ViewHolder holder;

        if (convertView == null) {

            view = LayoutInflater.from(mContext).inflate(R.layout.reported_card_horizontal_image , viewGroup,false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.adress.setText(reports.get(i).getAddress());
        holder.category.setText(reports.get(i).getCategory().getName());
        holder.description.setText(reports.get(i).getDescription());
        holder.status.setText(reports.get(i).getStatus());

        String dateReported = reports.get(i).getDate_uploaded();
        if (dateReported.length() < 10){
            holder.reportDate.setText(dateReported);
        }
        else{
            holder.reportDate.setText(dateReported.substring(0,10));
        }

        String dateSolved = reports.get(i).getDate_resolved();
        if (dateSolved.equals("null")){
            holder.solvedDate.setText("           /");
        }
        else if (dateSolved.length() < 10){
            holder.solvedDate.setText(dateSolved);
        }
        else{
            holder.solvedDate.setText(dateSolved.substring(0,10));
        }

        holder.likes.setText(reports.get(i).getUpvotes().toString());
        holder.likeButton.setTag(i);


        //Picasso.get().load(reports.get(i).getImageUriList().get(0)).centerCrop().fit().into(holder.image);
        if (reports.get(i).getImageUriList().size()>0) {
            Picasso.get()
                    .load(ApiCall.PHOTO_ENDPOINT+(reports.get(i).getImageUriList().get(0)).toString())
                    .centerCrop().fit().into(holder.image);
            System.out.println(ApiCall.PHOTO_ENDPOINT+(reports.get(i).getImageUriList().get(0)).toString());

        }else{
            Picasso.get()
                    .load(R.drawable.no_image)
                    .centerCrop().fit().into(holder.image);
        }

        holder.likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Report report = reports.get((int)view.getTag());
                report.setUpvotes(report.getUpvotes()+1);
                ReportsListAdapter.this.notifyDataSetChanged();
                view.setEnabled(false);
                Toast.makeText(mContext, "lajkali ste", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    private class ViewHolder {
        private TextView adress;
        private TextView category;
        private TextView description;
        private TextView status;
        private TextView reportDate;
        private TextView solvedDate;
        private TextView likes;

        private ImageButton likeButton;
        private ImageView image;

        public ViewHolder(View view) {
            this.adress = (TextView) view.findViewById(R.id.title);
            this.category = (TextView) view.findViewById(R.id.category);
            this.description = (TextView) view.findViewById(R.id.description);
            this.status = (TextView) view.findViewById(R.id.status);
            this.reportDate = (TextView) view.findViewById(R.id.date_uploaded);
            this.solvedDate = (TextView) view.findViewById(R.id.date_resolved);
            this.likes = (TextView) view.findViewById(R.id.likes);

            this.likeButton = (ImageButton) view.findViewById(R.id.wrench);
            this.image = (ImageView) view.findViewById(R.id.image);
        }
    }
}
