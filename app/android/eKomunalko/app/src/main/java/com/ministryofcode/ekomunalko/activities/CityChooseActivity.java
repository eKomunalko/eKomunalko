package com.ministryofcode.ekomunalko.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ministryofcode.ekomunalko.R;
import com.ministryofcode.ekomunalko.api.ApiCall;
import com.ministryofcode.ekomunalko.api.Category;
import com.ministryofcode.ekomunalko.api.City;

import java.util.ArrayList;
import java.util.List;

public class CityChooseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeGUI();
        getCities();
        //setContentView(R.layout.activity_city_choose);
    }

    public void initializeGUI() {

        // Sets title.
        setTitle(R.string.app_name);

        // For now just show with location turned off.
        // Later there will be a check to see if location turned on.
        setContentView(R.layout.city_choose_location_off);
    }

    /**
     * Sets the title of the activity in the middle of the toolbar.
     * @param title
     */
    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);

        super.setTheme(R.style.Toolbar);

        TextView textView = new TextView(this);
        textView.setText(title);
        textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        textView.setGravity(Gravity.CENTER_HORIZONTAL);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.title_size));
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(textView);
    }

    public void turnOnLocation(View view) {
        // Nije jos implementirano!
        Toast.makeText(this, "Nije jos implementirano, koristite gumbe.", Toast.LENGTH_SHORT).show();
    }

    public void getCities(){
        List<City> list = ApiCall.getCities(ApiCall.CITY_ENDPOINT);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();

        for (City c : list){
            setButton(c.getName());
            editor.putString(c.getName(), c.getAbbrev());

        }

        editor.apply();
    }

    public void setButton(String name){

        Button button = new Button(this);
        button.setText(name);
        button.setTextColor(getResources().getColor(R.color.colorPrimary));
        button.setTextSize(28.5f);
        button.setAllCaps(false);
        button.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        LinearLayout l = findViewById(R.id.linearLayout);
        l.addView(button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                citySelected(view);

            }
        });

    }





    /**
     * Method is called on click of any city button.
     * @param view that will be used to determine which button in pressed.
     */
    public void citySelected(View view) {
        // Get the string in the pressed button.
        String chosenCity = ((Button) view).getText().toString();

        // Get the shared preferences.
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        // Get editable preferences.
        SharedPreferences.Editor editor = prefs.edit();
        // Put new chosen city in preferences.
        editor.putString(getString(R.string.city_location_key), chosenCity);
        // Save changes.
        editor.apply();

        // After choosing city and saving it, open main app activity.
        Intent mainAppIntent = new Intent(this, Main2Activity.class);
        startActivity(mainAppIntent);
    }
}
