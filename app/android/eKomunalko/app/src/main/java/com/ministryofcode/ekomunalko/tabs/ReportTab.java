package com.ministryofcode.ekomunalko.tabs;

import android.Manifest;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.icu.util.ULocale;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ministryofcode.ekomunalko.BuildConfig;
import com.ministryofcode.ekomunalko.CategoriesAdapter;
import com.ministryofcode.ekomunalko.R;
import com.ministryofcode.ekomunalko.activities.Main2Activity;
import com.ministryofcode.ekomunalko.api.ApiCall;
//import com.ministryofcode.ekomunalko.api.Category;
import com.ministryofcode.ekomunalko.api.Category;
import com.ministryofcode.ekomunalko.api.City;
import com.ministryofcode.ekomunalko.api.Report;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Class represents tab used for reporting problems.
 */

//All on click events using any of the buttons in report tab should be implemented here.
public class ReportTab extends Fragment {

    /**
     * Int that represents what request code we send and expect to receive when using ImageCapture. (When taking picture with camera)
     */
    private static final int REQUEST_IMAGE_CAPTURE = 1;

    /**
     * Int that represents what request code we send and expect to receive when using GetContent. (When loading images from gallery)
     */
    private static final int REQUEST_IMAGES_FROM_GALLERY = 2;

    /**
     * Number of images that are going to be displayed to the user per row.
     */
    private static final int NUMBER_OF_IMAGE_PER_ROW = 2;

    /**
     * List that hold all images that the user is planing to use to report a problem.
     * These are the images that should be displayed on report problem screen.
     */
    private List<Bitmap> imageList;

    //References to parts of the report screen that is defined in the report_problem.xml.
    //Include stuff like cameraButton, galleryButton, reportButton...
    private LinearLayout imagesLayout;
    private ImageButton cameraButton;
    private ImageButton galleryButton;
    private Button reportButton;
    private AutoCompleteTextView addressInputView;
    private TextView descriptionView;
    private Spinner categoriesDropDown;
    private Uri imageUri;

    /**
     * Method creates fragment view from xml file and returns it for display.
     *
     * @param inflater
     * @param container - parent of fragment view (in our case Main2Activity viewGroup)
     * @param savedInstanceState
     * @return xml file as view to be displayed.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Inflate view using inflater from specified xml file and add it to parent view group.
        View rootView = inflater.inflate(R.layout.report_problem, container, false);

        this.imageList = new ArrayList<>();

        //Getting references defined in the report_problem.xml
        this.imagesLayout = rootView.findViewById(R.id.imageLayout);
        this.addressInputView = rootView.findViewById(R.id.addressInputView);
        this.descriptionView = rootView.findViewById(R.id.descriptionTextView);
        this.cameraButton = rootView.findViewById(R.id.cameraButton);
        this.galleryButton = rootView.findViewById(R.id.galleryButton);
        this.reportButton = rootView.findViewById(R.id.reportButton);
        this.categoriesDropDown = rootView.findViewById(R.id.dynamic_spinner);

        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
            }
        });

        galleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadImageFromGallery();
            }
        });

        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reportProblem();
            }
        });

        addAllImagesOnScreen();
        fillAvailableCategories();
        return rootView;
    }

    private void fillAvailableCategories() {
        // TODO Replace this with actual API call for categories
        List<Category> cat = ApiCall.getCategories(ApiCall.CATEGORIES_ENDPOINT);

        //ArrayAdapter<Category> adapter = new ArrayAdapter<String>(getContext(), R.layout.category_item, cat);
        CategoriesAdapter adapter = new CategoriesAdapter(getContext(), cat);
        categoriesDropDown.setAdapter(adapter);
    }

    String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        Date timeStamp = new Date(System.currentTimeMillis());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(getActivity(), "Error while taking picture", Toast.LENGTH_SHORT).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                imageUri=photoURI;
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    /**
     * Method starts intent to get a new picture by opening device's default camera.
     */
    public void takePicture() {

        closeAllInputs();

        //PermissionsUtil.askForCameraPermission(getActivity());
        /*if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA}, 0);
        }*/

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{Manifest.permission.CAMERA}, 0);
            return;
        }

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            return;
        }

        dispatchTakePictureIntent();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            dispatchTakePictureIntent();
        }
    }

    /**
     * Method starts intent to get picture from gallery.
     */
    public void loadImageFromGallery() {

        closeAllInputs();

        Intent gallery = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, REQUEST_IMAGES_FROM_GALLERY);
    }

    /**
     * Method is used to close all user input, like keyboard used to input address or description.
     * If those keyboard are not closed app crashes when returned for that taking picture or loading from gallery.
     */
    public void closeAllInputs() {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.addressInputView.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(this.descriptionView.getWindowToken(), 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //resultCode tells us which activity result did we get. Our app has more that one possible intent type
        // (take picture with camera or load images from gallery) and they all call this method so we need to differentiate them.

        //If resultCode is the one that represents picture take with camera result.
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == getActivity().RESULT_OK) {

            addUriToUriList(imageUri);
            addImageOnScreen(imageUri);

            //If resultCode is the one that represents image loaded from gallery.
        } else  if (resultCode == getActivity().RESULT_OK && requestCode == REQUEST_IMAGES_FROM_GALLERY && data != null) {

            Uri selectedImage = data.getData();
            addUriToUriList(selectedImage);

            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            //BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inSampleSize = 3;
            //options.inJustDecodeBounds = false;
            //Bitmap imageBitmap = BitmapFactory.decodeFile(picturePath, options);

            //addImageOnScreen(imageBitmap);
            addImageOnScreen(selectedImage);
        }

    }

    /**
     * Adds all Bitmaps currently stored in imageList to display using addImageOnScreen method.
     */
    public void addAllImagesOnScreen() {
        List<Uri> uriList = ((Main2Activity)getActivity()).getUriList();

        for (Uri uri : uriList) {
            addImageOnScreen(uri);
        }
    }

    /**
     * Method serves as a generic way to add images to display for the user. It is designed to be used to add all Bitmaps from imageList to screen
     * or to add one new image when user takes another picture.
     *
     * @param image - image that is going to be added to screen
     */
    private void addImageOnScreen(Uri image) {
        final ImageView newImageView = createNewImageView(imagesLayout);
        Picasso.get().load(image.toString()).fit().centerCrop().into(newImageView);
    }

    /**
     * Method dynamically creates new ImageView that is going to used for image display. It checks if last row (LinearLayout in imageLayout) has max number of images.
     * If it does then it creates a new LinearLayout (new row) and adds it in there.
     *
     * @param imageLayout - parent layout. Should always be imagesLayout but left as an argument for possible reuse later.
     * @return - reference to newly created ImageView
     */
    private ImageView createNewImageView(LinearLayout imageLayout) {
        int numberOfChildren = imageLayout.getChildCount();

        if (numberOfChildren == 0 ||
                ((LinearLayout) imageLayout.getChildAt(numberOfChildren - 1)).getChildCount() == NUMBER_OF_IMAGE_PER_ROW) {

            createNewLinearLayout(imageLayout);
        }

        LinearLayout imageRowLayout = (LinearLayout) imageLayout.getChildAt(imageLayout.getChildCount() - 1);

        ImageView newImage = new ImageView(getActivity());
        newImage.setAdjustViewBounds(true);
        DisplayMetrics display = this.getResources().getDisplayMetrics();
        newImage.setMaxWidth((int)(display.widthPixels / 2.18));
        newImage.setMaxHeight(newImage.getMaxWidth());
        newImage.setMinimumWidth(newImage.getMaxWidth());
        newImage.setMinimumHeight(newImage.getMaxWidth());

        //////////
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.weight = 0.5f;

        params.setMargins(14, 0, 10, 0);

        imageRowLayout.addView(newImage, params);

        return newImage;
    }

    /**
     * Method is used to dynamically add new LinearLayout to a parent layout. It is used because our app displays two images per row.
     * Those two images are placed in a LinearLayout and then multiple LinearLayouts are placed in one parent LinearLayout that hold all the images.
     *
     * @param parent - parent LinearLayout to which the new one is going to be added.
     *               It should always be imagesLayout but left as an argument for possible reuse later.
     */
    private void createNewLinearLayout(LinearLayout parent) {
        LinearLayout newLayout = new LinearLayout(getActivity());
        newLayout.setPadding(0, 10, 0, 0);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.weight = 1;
        params.setMargins(0, 0, 0, 0);

        newLayout.setOrientation(LinearLayout.HORIZONTAL);
        newLayout.setLayoutParams(params);

        parent.addView(newLayout, params);
    }

    private void addUriToUriList(Uri uri) {
        ((Main2Activity)getActivity()).getUriList().add(uri);
    }

    /**
     * Method called on click of the 'PRIJAVI' button.
     */
    public void reportProblem() {

        List<Uri> uriList = ((Main2Activity)getActivity()).getUriList();
        String address = addressInputView.getText().toString();
        String description = descriptionView.getText().toString();

        if (description.length() > 255) {
            Toast.makeText(getActivity(), "Opis je predug. Dozvoljeno je maksimalno 255 znakova.", Toast.LENGTH_LONG).show();
            return;
        }

        if (((Category)categoriesDropDown.getSelectedItem()).getName().equals("Odaberi kategoriju")) {
            Toast.makeText(getActivity(), "Molimo odaberite kategoriju.", Toast.LENGTH_LONG).show();
            return;
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String cityName = prefs.getString(getString(R.string.city_location_key),null);
        String cityAbbrev = prefs.getString(cityName,null);
        City city = new City(null,cityName,cityAbbrev);

        Category category = (Category)categoriesDropDown.getSelectedItem();

        Report report = new Report(uriList,address,description,city,category);

        System.out.println(uriList.size());

        ApiCall.postReport(ApiCall.REPORTS_ENDPOINT,report, getActivity().getApplicationContext());


        // TODO Attempt to remove data once user clicks report!

        imagesLayout.removeAllViews();
        imageList.clear();
        ((Main2Activity)getActivity()).getUriList().clear();
        addressInputView.setText("");
        descriptionView.setText("");

        Toast.makeText(getContext(), "Prijava poslana.", Toast.LENGTH_LONG).show();

    }
}
