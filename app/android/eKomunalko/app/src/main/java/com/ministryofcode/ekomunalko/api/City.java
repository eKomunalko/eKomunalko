package com.ministryofcode.ekomunalko.api;

/**
 * Created by dojitza on 09.05.18..
 */

public class City {
    private Integer id;
    private String name;
    private String abbrev;

    public City(Integer id, String name, String abbrev) {
        this.id = id;
        this.name = name;
        this.abbrev = abbrev;
    }

    @Override
    public String toString() {
        return "City{" +
                "id='" + id + '\'' +
                "name='" + name + '\'' +
                ", abbrev='" + abbrev + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public String getAbbrev() {
        return abbrev;
    }
}
