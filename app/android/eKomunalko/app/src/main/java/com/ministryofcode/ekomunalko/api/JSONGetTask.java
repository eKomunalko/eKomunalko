package com.ministryofcode.ekomunalko.api;

import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * Created by dojitza on 30.03.18..
 *
 * A simple AsyncTask that performs a http GET request to a provided url, recieving a JSON
 *
 */

class JSONGetTask extends AsyncTask<String, Void, JSONArray> {

    private HttpURLConnection con;

    @Override
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected JSONArray doInBackground(String... urls) {
        String urlParameters =  urls[1];
        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);

        try {
            String to_be_url = new String(urls[0]);
            if (urlParameters!=""){
                to_be_url+=('?'+urls[1]);
            }
            URL myurl = new URL(to_be_url);
            System.out.println(to_be_url);

            con = (HttpURLConnection) myurl.openConnection();

            con.setDoOutput(false);
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Java client");
            con.setRequestProperty("Content-Type", "application/json");

            StringBuilder content;

            try (BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()))) {

                String line;
                content = new StringBuilder();

                while ((line = in.readLine()) != null) {
                    content.append(line);
                    content.append(System.lineSeparator());
                }
            }
            return new JSONArray(content.toString());

        } catch (ProtocolException e) {
            e.printStackTrace();
            return null;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;

    }
}
