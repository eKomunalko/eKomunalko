package com.ministryofcode.ekomunalko.tabs;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ministryofcode.ekomunalko.EndlessScrollListener;
import com.ministryofcode.ekomunalko.R;
import com.ministryofcode.ekomunalko.ReportsListAdapter;
import com.ministryofcode.ekomunalko.api.ApiCall;
import com.ministryofcode.ekomunalko.api.Report;

import java.util.List;

/**
 * Class represents tab used for viewing already reported problems.
 */

//All on click events using any of the buttons in reported tab should be implemented here.
public class ReportedTab extends Fragment {

    private ReportsListAdapter adapter;
    private Boolean reachedEndOfData = false;

    private String currentCity;

    /**
     * Method creates fragment view from xml file and returns it for display.
     * @param inflater
     * @param container parent of fragment view
     * @param savedInstanceState
     * @return xml file as view to be displayed.
     */


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Inflate view using inflater from specified xml file and add it to parent view group.
        View rootView = inflater.inflate(R.layout.reported_problems, container, false);

        final SwipeRefreshLayout refreshLayout = ((SwipeRefreshLayout)rootView.findViewById(R.id.swiperefresh));
        /*
         * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
         * performs a swipe-to-refresh gesture.
         */

        //Getting city
        currentCity = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.city_location_key), null);

        final List<Report> reports = ApiCall.getReports(ApiCall.REPORTS_ENDPOINT,"amount=5&start=0&city="+currentCity);
        adapter = new ReportsListAdapter(getContext(),reports);
        final ListView listView = rootView.findViewById(R.id.list_view);

        listView.setAdapter(adapter);

        listView.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {

                if (reachedEndOfData){
                    return false;
                }

                Integer lastId = adapter.getReports().get(adapter.getReports().size()-1).getId();
                List<Report> reports = ApiCall.getReports(ApiCall.REPORTS_ENDPOINT,"amount=5&start="+lastId+"&city="+currentCity);

                if (reports==null){
                    return true;
                }
                if (reports.size()==0){
                    reachedEndOfData =true;
                    return false;
                }
                else{
                    adapter.getReports().addAll(reports);
                    adapter.notifyDataSetChanged();
                    return false;
                }




            }
        });

        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        // This method performs the actual data-refresh operation.
                        // The method calls setRefreshing(false) when it's finished.

                        List<Report> reports = ApiCall.getReports(ApiCall.REPORTS_ENDPOINT,"amount=5&start=0&city="+currentCity);
                        adapter.getReports().clear();
                        adapter.getReports().addAll(reports);
                        adapter.notifyDataSetChanged();
                        refreshLayout.setRefreshing(false);
                        reachedEndOfData =false;
                    }
                }
        );

        return rootView;
    }
}
