package com.ministryofcode.ekomunalko.api;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

class JSONPostTask extends AsyncTask<Report, Void, String> {

    private final String urlString;

    public JSONPostTask(String urlString) {
        this.urlString = urlString;
    }

    private Context context;
    public void setContext(Context context){
        if (context == null) {
            throw new IllegalArgumentException("Context can't be null");
        }
        this.context = context;
    }

    @Override
    protected String doInBackground(Report... reports) {

        JSONObject json = JsonUtil.reportToJSon(reports[0],context);

        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setRequestProperty("Accept","application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);

            Log.i("JSON", json.toString());
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
            os.writeBytes(json.toString());

            os.flush();
            os.close();

            Log.i("STATUS", String.valueOf(conn.getResponseCode()));
            Log.i("MSG" , conn.getResponseMessage());

            conn.disconnect();

            return conn.getResponseMessage();

        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }

    }

}
