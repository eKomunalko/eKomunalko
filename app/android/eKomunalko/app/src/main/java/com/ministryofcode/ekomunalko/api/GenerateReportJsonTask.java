package com.ministryofcode.ekomunalko.api;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

class GenerateReportJsonTask extends AsyncTask<Report, Void, JSONObject> {

    private final Context context;

    public GenerateReportJsonTask(Context context) {
        this.context = context;
    }

    @Override
    protected JSONObject doInBackground(Report... reports) {
        JSONObject json = JsonUtil.reportToJSon(reports[0], context);
        return json;
    }
}
