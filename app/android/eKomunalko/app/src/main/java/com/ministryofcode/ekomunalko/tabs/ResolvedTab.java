package com.ministryofcode.ekomunalko.tabs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ministryofcode.ekomunalko.R;

/**
 * Class represents tab used for viewing already reported problems.
 */

//All on click events using any of the buttons in finished tab should be implemented here.
public class ResolvedTab extends Fragment {

    /**
     * Method creates fragment view from xml file and returns it for display.
     * @param inflater
     * @param container parent of fragment view
     * @param savedInstanceState
     * @return xml file as view to be displayed.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Inflate view using inflater from specified xml file and add it to parent view group.
        View rootView = inflater.inflate(R.layout.resolved_problems, container, false);
        return rootView;
    }
}
