package com.ministryofcode.ekomunalko.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ministryofcode.ekomunalko.R;
import com.ministryofcode.ekomunalko.tabs.ResolvedTab;
import com.ministryofcode.ekomunalko.tabs.ReportTab;
import com.ministryofcode.ekomunalko.tabs.ReportedTab;

import java.util.ArrayList;
import java.util.List;

import static android.view.Gravity.apply;

public class Main2Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private ActionBarDrawerToggle mToggle;
    private DrawerLayout mDrawerLayout;

    private NavigationView navigationView;

    public List<Uri> imageUriList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        setTitle(R.string.app_name);

        // Check is current city set!
        checkForCurrentCity();

        //Initializing mDrawerLayout by referencing it's id defined in activity_main2.xml
        mDrawerLayout = (DrawerLayout) findViewById(R.id.main_content);

        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        // For now until we decide how to handle cities!
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String city = prefs.getString(getString(R.string.city_location_key), null);
        if (!city.equals("null"))
            Toast.makeText(this, "Trenutna lokacija: " + city, Toast.LENGTH_LONG).show();

        if (imageUriList == null) {
            imageUriList = new ArrayList<>();
        }
    }

    /**
     * Sets the title of the activity in the middle of the toolbar.
     * @param title
     */
    @Override
    public void setTitle(CharSequence title) {
      //  super.setTitle(title);
        super.getApplicationContext().setTheme(R.style.Toolbar);
        RelativeLayout layout = new RelativeLayout(this);
        TextView titleView = new TextView(this);
        TextView abbrev = new TextView(this);

        abbrev.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        abbrev.setText("Bok");
        abbrev.setGravity(Gravity.END);
        abbrev.setTextColor(getResources().getColor(R.color.colorRed));
        abbrev.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.abbreviation_size));
        abbrev.setTypeface(null, Typeface.BOLD);

        titleView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        titleView.setText(title + "        ");
        titleView.setGravity(Gravity.CENTER);
        titleView.setTypeface(null, Typeface.BOLD);

        // apply(Gravity.CENTER, 5, 10, null, null);
        titleView.setTextColor(Color.parseColor("#006064"));
        titleView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.title_size));


        layout.addView(titleView);
        layout.addView(abbrev);

       // layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER));

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(layout);
    }

    // For now leave empty.
    public void setCurrentCityLabel(String label) {

        ((TextView)((RelativeLayout)getSupportActionBar().getCustomView()).getChildAt(1)).setText(label.toUpperCase());
    }

    public List<Uri> getUriList() {
        return imageUriList;
    }

    // u ovoj funkciji se definira onaj desni meni sa settings opcijom
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }
    @Override
    public boolean onNavigationItemSelected (@NonNull MenuItem item){
        if (item.getItemId() == R.id.nav_manual){
            Intent navMan = new Intent(this, ManualActivity.class);
            startActivity(navMan);
        }

        /*else if (item.getItemId() == R.id.nav_settings){
            Intent navSett = new Intent(this, SettingsActivity.class);
            startActivity(navSett);
        }*/

        else if (item.getItemId() == R.id.nav_about){
            Intent navAb = new Intent(this, AboutActivity.class);
            startActivity(navAb);
        }

        else if (item.getItemId() == R.id.nav_change_town){
            Intent citychoose = new Intent(this, CityChooseActivity.class);
            startActivity(citychoose);

        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        //U ovoj metodi ces trebat definirat sta se desava na klik tog gumba!!
        if (mToggle.onOptionsItemSelected(item)){
            return true;

        }


        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }



        return super.onOptionsItemSelected(item);
    }

    /**
     * Method is going to be run each time the user starts app.
     * If user does not have a specified city location method is
     * going to start an activity that asks this information from user.
     */
    public void checkForCurrentCity() {
        // This gets the user preferences (basically a xml file that can store key value pairs that
        // persists between runs of the app). Using default shared preferences means default file name.
        // It is possible to specify different name if necessary.
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String cityName = prefs.getString(getString(R.string.city_location_key), null);
        String abbrev = prefs.getString(cityName,null);
        if (abbrev != null) {
            // Current city location is not null, so put city name in top right corner and return.
            setCurrentCityLabel(abbrev);
            return;
        } else {
            // Current city location is null, so redirect to CityChooseActivity.
            Intent cityChooseIntent = new Intent(this, CityChooseActivity.class);
            startActivity(cityChooseIntent);
        }
    }


    //Deleted PlaceholderFragment class for empty tabs.
    //Now using our implementation of fragments.
    //Implementation in app/java/com.ministryofcode.ekomunalko.tabs

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.

            switch (position){
                case 0:
                    ReportTab reportTab = new ReportTab();
                    return reportTab;
                case 1:
                    ReportedTab reportedTab = new ReportedTab();
                    return reportedTab;
                //case 2:
                //    ResolvedTab resolvedTab = new ResolvedTab();
                //    return resolvedTab;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }
    }
}
