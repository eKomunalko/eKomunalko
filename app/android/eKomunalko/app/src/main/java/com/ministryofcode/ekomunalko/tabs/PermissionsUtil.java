package com.ministryofcode.ekomunalko.tabs;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Josip on 11.5.2018..
 */

public class PermissionsUtil {

    final private static int CAMERA_REQUEST_CODE = 42;

    /**
     * Method ask the user for camera permission at runtime.
     * @param activity - activity
     */
    public static void askForCameraPermission(FragmentActivity activity) {
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.CAMERA}, 0);
        }
    }
}
