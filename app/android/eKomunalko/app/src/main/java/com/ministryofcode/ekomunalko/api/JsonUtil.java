package com.ministryofcode.ekomunalko.api;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import android.net.Uri;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by dojitza on 09.04.18..
 */

public class JsonUtil {

    public static JSONObject reportToJSon(Report report, Context context) {

        try {

            /*

            {
                "address":"neka adresa",
                "description":"neki opis",
                "city":{"name":"Zagreb"},
                "category":{"name":"Ceste"},
                "status":"nije bitno",

                "photos":
                    [
                        {"photo":slika1,"is_before":"True"},
                        {"photo":slika2,"is_before":"True"}
                    ]
             }

             */

            // Here we convert Java Object to JSON
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("address", report.getAddress());
            jsonObj.put("description", report.getDescription());

            JSONObject jsonCity = new JSONObject();
            jsonCity.put("name", report.getCity().getName());
            jsonCity.put("abbrev",report.getCity().getAbbrev());
            jsonObj.put("city",jsonCity);


            JSONObject jsonCategory = new JSONObject();
            jsonCategory.put("name", report.getCategory().getName());
            jsonObj.put("category",jsonCategory);

            JSONObject jsonStatus = new JSONObject();
            jsonStatus.put("name","Zaprimljeno");
            jsonObj.put("status", jsonStatus);



            JSONArray jsonImgs = new JSONArray(); // an array for images

            int i = 1;
            List<Uri> imageUriList = new LinkedList<>(report.getImageUriList());
            for (Uri imageUri : imageUriList){

                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), imageUri);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                byte[] byteArray = stream.toByteArray();
                bitmap.recycle();

                JSONObject imgObj = new JSONObject();
                String base64_encoded_image = Base64.encodeToString(byteArray, Base64.DEFAULT);
                imgObj.put("photo", base64_encoded_image);
                imgObj.put("is_before", "True");

                jsonImgs.put(imgObj);

            }

            // We add the array to the main object
            jsonObj.put("photos", jsonImgs);
            
            return jsonObj;


        }
        catch(JSONException | IOException ex) {
            ex.printStackTrace();
        }

        return null;

    }

    public static List<Report> JsonToReports(JSONArray reports_array){
        /*
        [
                {
                    "id":1,
                        "address":"sesvete 1",
                        "description":"RUPETINA!!!!!",
                        "city":{"id":1,"name":"zagreb"},
                    "category":{"id":1,"name":"asdasd"},
                    "status":{"id":1,"name":"nije"},
                    "photos":[],"upvotes":0,
                        "date_uploaded":"2018-05-08T14:17:30Z",
                        "date_resolved":"2018-05-08T14:17:34Z"
                }
        ]
        */

        List<Report> reports_list = new LinkedList<>();
        try {
            for (int i = 0;i<reports_array.length(); i++) {
                JSONObject report_object = reports_array.getJSONObject(i);
                Integer id = report_object.getInt("id");
                String address = report_object.getString("address");
                String description = report_object.getString("description");
                Integer cityId = report_object.getJSONObject("city").getInt("id");

                String cityName = report_object.getJSONObject("city").getString("name");
                String cityAbb = report_object.getJSONObject("city").getString("abbrev");
                City city = new City(cityId,cityName,cityAbb);

                Integer categoryId = report_object.getJSONObject("category").getInt("id");
                String categoryName = report_object.getJSONObject("category").getString("name");
                Category category = new Category(categoryId,categoryName);

                String status = report_object.getJSONObject("status").getString("name");
                JSONArray photosArray = report_object.getJSONArray("photos");
                Integer upvotes = report_object.getInt("upvotes");
                List<Uri> photosUriList= new LinkedList<>();

                for (int j = 0;j<photosArray.length(); j++) {
                    JSONObject photoObject = photosArray.getJSONObject(j);
                    photosUriList.add(Uri.parse(photoObject.getString("photo")));
                }
                String date_uploaded = report_object.getString("date_uploaded");
                String date_resolved = report_object.getString("date_resolved");

                Report report = new Report(id,photosUriList,address,description,city,category,status,date_uploaded,date_resolved,upvotes);

                reports_list.add(report);
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }

        return reports_list;
    }

    public static List<City> JsonToCities(JSONArray cities_array){
        /*
        [
            {
                "id": 1,
                "name": "Zagreb"
                "abbrev": "zg"
            },
            {
                "id": 2,
                "name": "Karlovac"
                "abbrev": "ka"
            }
]
        */

        List<City> cities_list = new LinkedList<>();
        try {
            for (int i = 0;i<cities_array.length(); i++) {
                JSONObject city_object = cities_array.getJSONObject(i);
                Integer id = city_object.getInt("id");
                String name = city_object.getString("name");
                String abbrev = city_object.getString("abbrev");

                City city = new City(id,name,abbrev);

                cities_list.add(city);
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }

        return cities_list;
    }

    public static List<Category> JsonToCategories(JSONArray categories_array){
        /*
        [
            {
                "id": 2,
                "name": "Ceste"
            },
            {
                "id": 1,
                "name": "asdasd"
            }
        ]
        */

        List<Category> categories_list = new LinkedList<>();
        try {
            for (int i = 0;i<categories_array.length(); i++) {
                JSONObject category_object = categories_array.getJSONObject(i);
                Integer id = category_object.getInt("id");
                String name = category_object.getString("name");

                Category category = new Category(id,name);

                categories_list.add(category);
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }

        return categories_list;
    }
}
