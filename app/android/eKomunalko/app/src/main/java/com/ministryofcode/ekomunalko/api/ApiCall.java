package com.ministryofcode.ekomunalko.api;

import android.content.Context;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Dojitza on 10.3.2018..
 * A collection of web api methods to communicate with the backend server
 */

public abstract class ApiCall {

    public static final String BASE_URL = "http://35.197.198.17";
    public static final String API = BASE_URL + ":8000/api";
    public static final String CITY_ENDPOINT = API + "/cities";
    public static final String CATEGORIES_ENDPOINT = API + "/categories";
    public static final String REPORTS_ENDPOINT = API + "/entries";
    public static final String PHOTO_ENDPOINT = BASE_URL + ":8080/";

    /**
     * Performs a http POST request using a provided url and parameters.
     *
     *
     * @param urlString a string of the url to which the POST request will be sent
     *
     * @param urlParameters a string containing post parameters that will be sent with the request
     *                      the format is as follows:
     *                      parameter1=value1&parameter2=value2...
     *                      the '&' character is used as a separator between parameter=value pairs
     *
     * @return a string of the http POST server response
     */

    public static String postRequest(final String urlString, final String urlParameters){

        PostTask postTask = new PostTask();
        String result = null;

        try {
            // "http://10.0.2.2:8000/test/"
            result = postTask.execute(urlString, urlParameters).get();

        } catch (InterruptedException e) {
            e.printStackTrace();
            return "failure";

        } catch (ExecutionException e) {
            e.printStackTrace();
            return "failure";
        }

        return result;
    }

    /**
     * Performs a http GET request using a provided url and parameters.
     *
     *
     * @param urlString a string of the url to which the POST request will be sent
     *
     * @param urlParameters a string containing get parameters that will be sent with the request
     *                      appended to the url. the format is as follows:
     *                      parameter1=value1&parameter2=value2...
     *                      the '&' character is used as a separator between parameter=value pairs
     *
     * @return a string of the http GET server response
     */

    public static String getRequest(final String urlString, final String urlParameters){
        GetTask getTask = new GetTask();
        String result = null;

        try {
            // "http://10.0.2.2:8000/test/"
            result = getTask.execute(urlString, urlParameters).get();

        } catch (InterruptedException e) {
            e.printStackTrace();
            return "failure";

        } catch (ExecutionException e) {
            e.printStackTrace();
            return "failure";
        }

        return result;
    }

    /**
     * Performs a POST call to the provided url with a json object containing all info required
     * for one report
     * @see com.ministryofcode.ekomunalko.api.Report
     *
     * @param urlString a string of the url to which the POST request will be sent
     *
     * @param report a Report object from which a json object will be constructed
     *
     * @param context application context required for loading images into json
     *
     * @return post request message
     */

    public static void postReport(final String urlString, Report report, Context context){

        //JSONObject json = new GenerateReportJsonTask(context).doInBackground(report);
        //Log.i("JSON",json.toString());

        JSONPostTask json_task = new JSONPostTask(urlString);
        json_task.setContext(context);
        json_task.execute(report);

        return;
    }

    /**
     * Performs a GET request to the provided Url, anticipating a JSON response and parsing
     * it into a list of Reports.
     *
     * @see com.ministryofcode.ekomunalko.api.Report
     *
     * @param urlParameters consists of two parameters:
     *                     amount and start
     *                     amount: amount of reports to get
     *                     start: from which report to start
     *                     is in form: "amount=[value]&start=[value]"
     *
     * @param urlString a string of the url to which the GET request will be sent
     *
     * @return returns a list of Report objects
     */


    public static List<Report> getReports(final String urlString, final String urlParameters){
        JSONGetTask json_task = new JSONGetTask();

        try {

            //TODO pametnije napravit ovo, da se ne ceka dok se to izvede
            return JsonUtil.JsonToReports(json_task.execute(urlString,urlParameters).get());

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static List<City> getCities(final String urlString){
        JSONGetTask json_task = new JSONGetTask();

        try{
            return JsonUtil.JsonToCities(json_task.execute(urlString,"").get());
        }catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static List<Category> getCategories(final String urlString){
        JSONGetTask json_task = new JSONGetTask();

        try{
            return JsonUtil.JsonToCategories(json_task.execute(urlString,"").get());
        }catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

}
