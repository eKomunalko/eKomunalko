package com.ministryofcode.ekomunalko;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.ministryofcode.ekomunalko.api.Category;
import com.ministryofcode.ekomunalko.api.Report;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Josip on 11.5.2018..
 */

public class CategoriesAdapter extends BaseAdapter {

    private Context mContext;
    private List<Category> categories;

    public List<Category> getCategories() {
        return categories;
    }

    public CategoriesAdapter(Context context, List<Category> categories) {
        this.mContext = context;
        this.categories = categories;
        this.categories.add(0, new Category(0, "Odaberi kategoriju"));
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int i) {
        return this.categories.get(i);
    }

    @Override
    public long getItemId(int i) {
       return this.categories.get(i).getId();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        View view;

        if (convertView == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.category_item , viewGroup,false);
        } else {
            view = convertView;
        }


        ((TextView) view).setText(categories.get(i).getName());

        return view;
    }
}
