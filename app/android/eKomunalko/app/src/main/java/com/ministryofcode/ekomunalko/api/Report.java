package com.ministryofcode.ekomunalko.api;

import android.graphics.Bitmap;
import android.net.Uri;

import com.ministryofcode.ekomunalko.activities.Main2Activity;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by dojitza on 09.04.18..
 * Used to encapsulate a single report
 */

public class Report {
    Integer id;
    List<Uri> imageUriList;
    String address;
    String description;
    City city;
    Category category;
    String status;
    String date_uploaded;
    String date_resolved;
    Integer upvotes;

    public Report(List<Uri> imageUriList, String address, String description, City city, Category category) {
        this.imageUriList = new LinkedList<>(imageUriList);
        this.address = address;
        this.description = description;
        this.city = city;
        this.category = category;
    }

    public Report(Integer id, List<Uri> imageUriList, String address, String description, City city, Category category, String status, String date_uploaded, String date_resolved, Integer upvotes) {
        this.id = id;
        this.imageUriList = imageUriList;
        this.address = address;
        this.description = description;
        this.city = city;
        this.category = category;
        this.status = status;
        this.date_uploaded = date_uploaded;
        this.date_resolved = date_resolved;
        this.upvotes = upvotes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Uri> getImageUriList() {
        return imageUriList;
    }

    public void setImageUriList(List<Uri> imageUriList) {
        this.imageUriList = imageUriList;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate_uploaded() {
        return date_uploaded;
    }

    public void setDate_uploaded(String date_uploaded) {
        this.date_uploaded = date_uploaded;
    }

    public String getDate_resolved() {
        return date_resolved;
    }

    public void setDate_resolved(String date_resolved) {
        this.date_resolved = date_resolved;
    }

    public Integer getUpvotes() {
        return upvotes;
    }

    public void setUpvotes(Integer upvotes) {
        this.upvotes = upvotes;
    }

    @Override
    public String toString() {
        return "Report{" +
                "id=" + id +
                ", imageUriList=" + imageUriList +
                ", address='" + address + '\'' +
                ", description='" + description + '\'' +
                ", city='" + city + '\'' +
                ", category='" + category + '\'' +
                ", status='" + status + '\'' +
                ", date_uploaded='" + date_uploaded + '\'' +
                ", date_resolved='" + date_resolved + '\'' +
                '}';
    }
}
